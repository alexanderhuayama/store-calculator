(() => {
  const btnAdd = document.getElementById("btnAdd");
  const btnClose = document.getElementById("btnClose");
  const btnClear = document.getElementById("btnClear");
  const form = document.getElementById("form-product");
  const txtQuantity = document.getElementById("txtQuantity");
  const txtPrice = document.getElementById("txtPrice");
  const saleTotal = document.getElementById("sub-total-value");
  const bodyProducts = document.getElementById("body-products");

  function Product() {
    txtProductIndex;
    Object.defineProperty(this, "currentIndex", {
      get: function () {
        const input = document.getElementById("txtProductIndex");
        const content = input.value;
        if (isNaN(parseInt(content))) {
          return -1;
        }
        return parseInt(content);
      },
    });
    Object.defineProperty(this, "name", {
      get: function () {
        const input = document.getElementById("txtName");
        const content = input.value || "";
        return content.trim();
      },
    });

    Object.defineProperty(this, "quantity", {
      get: function () {
        const input = document.getElementById("txtQuantity");
        const quantity = input.value || 0;
        if (isNaN(quantity)) {
          return 0;
        }
        return parseFloat(quantity);
      },
    });

    Object.defineProperty(this, "price", {
      get: function () {
        const input = document.getElementById("txtPrice");
        const price = input.value || 0;
        if (isNaN(price)) {
          return 0;
        }
        return parseFloat(price);
      },
    });

    Object.defineProperty(this, "observation", {
      get: function () {
        const input = document.getElementById("txtObservation");
        const content = input.value || "";
        return content.trim();
      },
    });

    Object.defineProperty(this, "total", {
      get: function () {
        const total = this.quantity * this.price;
        return total.toFixed(2);
      },
    });
  }

  Product.prototype.toJSON = function () {
    const { name, quantity, price, observation, currentIndex } = this;
    return {
      name,
      quantity,
      price,
      observation,
      currentIndex,
    };
  };

  Product.prototype.getAll = function () {
    const storageData = localStorage.getItem("products");
    if (!storageData) {
      return [];
    }
    const products = JSON.parse(storageData);
    return products;
  };

  function toogleHiddenModal(action) {
    const modal = document.getElementById("modal-product");
    modal.classList[action]("hidden");
  }

  Product.prototype.add = function () {
    const products = this.getAll();
    const product = this.toJSON();
    const { currentIndex } = product;
    delete product.currentIndex;
    if (currentIndex >= 0) {
      products[currentIndex] = product;
    } else {
      products.push(product);
    }
    localStorage.setItem("products", JSON.stringify(products));
  };

  Product.prototype.loadByIndex = function (index) {
    const products = this.getAll();
    const product = products[index];
    const txtProductIndex = document.getElementById("txtProductIndex");
    const txtName = document.getElementById("txtName");
    const txtQuantity = document.getElementById("txtQuantity");
    const txtPrice = document.getElementById("txtPrice");
    const txtObservation = document.getElementById("txtObservation");
    txtProductIndex.value = index;
    txtName.value = product.name;
    txtQuantity.value = product.quantity;
    txtPrice.value = product.price;
    txtObservation.value = product.observation;
    toogleHiddenModal("remove");
    txtQuantity.focus();
  };

  Product.prototype.removeByIndex = function (index) {
    const products = this.getAll();
    products.splice(index, 1);
    localStorage.setItem("products", JSON.stringify(products));
    loadProducts();
  };

  function onChangeNumbers() {
    const product = new Product();
    saleTotal.innerHTML = `<span>S/.</span>${product.total}`;
  }

  function decimal(number) {
    return number.toFixed(2);
  }

  function renderSellTotal() {
    const sellResult = document.getElementById("sell-result");
    const products = new Product().getAll();
    const total = products
      .reduce((acc, product) => acc + product.quantity * product.price, 0)
      .toFixed(2);
    sellResult.innerHTML = `<span>S/. </span>${total}`;
  }

  function loadProducts() {
    const products = new Product().getAll();
    const totalItems = products.length;
    bodyProducts.innerHTML = "";
    products.reverse().forEach(function (product, index) {
      const row = document.createElement("tr");
      const position = document.createElement("td");
      const subtotal = document.createElement("td");
      const quantity = document.createElement("td");
      const price = document.createElement("td");
      const name = document.createElement("td");
      const btnWrapper = document.createElement("td");
      const btnRemove = document.createElement("button");
      position.textContent = totalItems - index;
      subtotal.textContent = decimal(product.quantity * product.price);
      quantity.textContent = decimal(product.quantity);
      price.textContent = decimal(product.price);
      name.textContent = product.name;
      btnRemove.setAttribute("data-index", totalItems - index - 1);
      btnRemove.setAttribute("data-action", "remove");
      btnRemove.textContent = "Remove";
      row.appendChild(position);
      row.appendChild(subtotal);
      row.appendChild(quantity);
      row.appendChild(price);
      row.appendChild(name);
      btnWrapper.appendChild(btnRemove);
      row.appendChild(btnWrapper);
      row.setAttribute("data-index", totalItems - index - 1);
      row.setAttribute("data-action", "edit");
      bodyProducts.appendChild(row);
    });
    renderSellTotal();
  }

  btnAdd.addEventListener("click", function () {
    toogleHiddenModal("remove");
    document.getElementById("txtName").focus();
  });

  btnClose.addEventListener("click", function () {
    toogleHiddenModal("add");
    form.reset();
    onChangeNumbers();
    loadProducts();
    document.getElementById("txtProductIndex").value = "";
  });

  btnClear.addEventListener("click", function () {
    form.reset();
    document.getElementById("txtName").focus();
    onChangeNumbers();
  });

  form.addEventListener("submit", function (e) {
    e.preventDefault();
    const product = new Product();
    product.add();
    btnClose.click();
  });

  txtQuantity.addEventListener("change", function () {
    onChangeNumbers();
  });

  txtPrice.addEventListener("change", function () {
    onChangeNumbers();
  });

  function removeItem(button) {
    const { index } = button.dataset;
    new Product().removeByIndex(index);
  }

  function loadProduct(row) {
    const { index } = row.dataset;
    new Product().loadByIndex(index);
  }

  function onClickRow(td) {
    const row = td.parentElement;
    const { action } = row.dataset;
    const actionMapping = {
      edit: loadProduct,
    };
    const currentAction = actionMapping[action];
    if (!currentAction) {
      return;
    }
    currentAction(row);
  }

  bodyProducts.addEventListener("click", function (e) {
    const { target } = e;
    const { nodeName } = target;
    const actionMapping = {
      TD: onClickRow,
      BUTTON: removeItem,
    };
    const currentAction = actionMapping[nodeName];
    if (!currentAction) {
      return;
    }
    currentAction(target);
  });

  loadProducts();
})();
